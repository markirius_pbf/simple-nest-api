import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(@Body() body) {
    return {
      "method": "GET",
      "msg": "Hello World"
    }
  }

  @Post()
  setHello(@Body() body) {
    return {
      "method": "POST",
      "msg": "Hello World"
    }
  }
}
